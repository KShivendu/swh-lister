beautifulsoup4
dateparser
dulwich
iso8601
launchpadlib
lxml
psycopg2
python_debian
repomd
requests
rpy2
setuptools
tenacity >= 6.2
testing.postgresql
